#!/bin/bash

# Author: https://gitlab.com/amalic
# based on this documentation https://docs.gitlab.com/charts/development/kind/

##############################################################################
# FUNCTIONS
##############################################################################

function check_for_required_commands {
  COMMAND_MISSING=false
  for COMMAND in $1; do
    if ! $(type "$COMMAND" &> /dev/null); then
      echo $COMMAND command is missing
      COMMAND_MISSING=true
    fi
  done
  if $COMMAND_MISSING; then
    echo EXITING DUE TO MISSING PREREQUISITES
    exit 1
  fi
}

function clone_pull_cd {
  DIRECTORY=$(basename "$1" .git)
  if [ -d "$DIRECTORY" ]; then
    cd "$DIRECTORY"
    git pull
  else
    git clone "$1"
    cd "$DIRECTORY"
  fi
}

function wait_for_address {
  echo -e "\nWaiting for "$1" to be available"
  until $(curl -k -1 --insecure --output /dev/null --silent --head --fail $1); do
    printf '.'
    sleep 1
  done
}

##############################################################################
# MAIN
##############################################################################

readonly BROWSER="google-chrome" # firefox or google-chrome

# check if all prerequisites are met
check_for_required_commands "git kind ip helm kubectl curl $BROWSER"

# clone or pull the gitlab charts repo and cd into directory
readonly GITLAB_CHARTS_REPO=https://gitlab.com/gitlab-org/charts/gitlab.git
clone_pull_cd $GITLAB_CHARTS_REPO

# create Kind cluster
kind create cluster --config examples/kind/kind-ssl.yaml
readonly MY_IP=$(ip route get 8.8.8.8 | sed -n '/src/{s/.*src *\([^ ]*\).*/\1/p;q}')
echo -e "\nLocal IP address: "$MY_IP"\n"

# install Gitlab-CE via helm
helm repo add gitlab https://charts.gitlab.io/
helm repo update
readonly DOMAIN="$MY_IP.nip.io"
helm upgrade --create-namespace -n gitlab --install gitlab gitlab/gitlab \
  --set global.edition=ce \
  --set global.hosts.domain=$DOMAIN \
  -f examples/kind/values-base.yaml \
  -f examples/kind/values-ssl.yaml

# print initial root password
# from https://docs.gitlab.com/charts/quickstart/#sign-in-to-gitlab
echo -e "\nInitial password for gitlab-user 'root' is '"\
$(kubectl get secret -n gitlab gitlab-gitlab-initial-root-password \
-ojsonpath='{.data.password}' | base64 --decode)"'\n"

# wait for address to be available ...
readonly ADDRESS="https://gitlab.$DOMAIN"
wait_for_address $ADDRESS
# ... and open in browser
eval $BROWSER $ADDRESS

# some final words before gracefully exiting
echo -e "\nDONE\nType 'kind delete cluster' to delete this installation"
exit 0
